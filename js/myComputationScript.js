

var param = {

    numeroSegmenti : 0,
    period : 0,
    degree: [3],
    numeroControlPoint : 0,
    continuity : [],
    ampiezzaSegmenti : [1],
    estremoA : 0,
    estremoB : 0,
    breakPoint : [],
    indicePrimoBreakPoint : 0,
    numeroBreakPoint : 0,
    indiceUltimoBreakPoint : 0,
    partizioneNodaleT : [],
    partizioneNodaleS : [],
    indiciPartizioneNodaleT : [],
    indiciPartizioneNodaleS : []
}

var paramDefault = {

    numeroSegmenti : 0,
    period : 0,
    degree: [3],
    numeroControlPoint : 0,
    continuity : [],
    ampiezzaSegmenti : [1],
    estremoA : 0,
    estremoB : 0,
    breakPoint : [],
    indicePrimoBreakPoint : 0,
    numeroBreakPoint : 0,
    indiceUltimoBreakPoint : 0,
    partizioneNodaleT : [],
    partizioneNodaleS : [],
    indiciPartizioneNodaleT : [],
    indiciPartizioneNodaleS : []
}

/**
 * Funzione che si occupa di creare la matrice bs
 * @method mainVD_MDspl_new
 * @param  {Integer}         controlPoint Numero di controlPoint inseriti dall utente (mai minori di 4)
 * @param  {Integer}         NUMBER_POINT Numero di punti in cui valutare l intervallo
 * @param  {Boolean}        flag         M informa se la curva e chiusa o no
 * @return {Matrix}      bs           Ritorna la matrice bs valutata nel numero di punti di valutazione
 */
function mainVD_MDspl_new(controlPoint,NUMBER_POINT,flag){

    define_MDspl_space(controlPoint, flag);


    var pointToValutate = gc_mesh_new(NUMBER_POINT);

    var bs = gc_MDbspl_valder(pointToValutate);
    console.log(bs);

    param = _.cloneDeep(paramDefault);

    return bs;

}

/**
 * Funzione che si occupa di fare la moltiplicazione tra la matrice bs e il vettore dei controlPoint
 * @method calculateMatrixControl
 * @param  {Matrix}               bs           Matrice di valutazione (m X nt) dove m e il numero di punti di valutazione e nt il numero di breakPoint
 * @param  {Array}               controlPoint controlPoint inseriti dall utente
 * @return {Array}                            Punti della curva
 */
function calculateMatrixControl(bs, controlPoint){
    var result = [];
    for(var r = 0 ; r < bs.length; r++){
        result[r] = [];
        var sum = 0;
        for(var c = 0 ; c < bs[r].length; c++){
            sum = sum + (bs[r][c][0] * controlPoint[c].x);
        }
        result[r].x = sum;
    }
    for(r = 0; r < bs.length; r++){
        sum = 0;
        for(c = 0 ; c < bs[r].length; c++){
            sum = sum + (bs[r][c][0] * controlPoint[c].y);
        }
        result[r].y = sum;
    }
    return result;
}

/**
 * Funzione che preso il numero di controlPoint e il flag per indicare se una curva e aperta o no
 * inizializza la struttura globale param dove sono definite tutte le informazioni sulla curva
 * @method define_MDspl_space
 * @param  {Array}           controlPoint  Vettore contenente elemnti nella forma [{x: int, y: int}];
 * @param  {Boolean}           flag           Indica se una curva e aperta o no
 * @return {Void}                        struttura param inizializzata
 */
function define_MDspl_space(controlPoint, period){

    for(var i = 4 ; i < controlPoint; i++){
        param.ampiezzaSegmenti.push(1);
        param.degree.push(3);
        param.continuity.push(2);
    }

    param.numeroSegmenti = param.ampiezzaSegmenti.length;
    param.numeroControlPoint = controlPoint;

    var temp = 0;
    for(var i = 0; i <= param.numeroSegmenti; i++){
        temp = _.sum(param.ampiezzaSegmenti.slice(0, i));
        param.breakPoint[i] = temp;
    }

    param.estremoA = param.breakPoint[0];
    param.estremoB = param.breakPoint[i - 1];
    param.indicePrimoBreakPoint = 0;
    param.indiceUltimoBreakPoint = param.numeroSegmenti;

    isClosedShape(period);
    partizioniNodali();
    param.numeroBreakPoint = param.breakPoint.length - 1;

}

/**
 * Funzione che aggiusta la struttura param in caso di una curva chiusa (DA FINIREEEE)
 * @method isClosedShape
 * @param  {Boolean}      flag variabile che mi esprime la volonta dell utente di fare una curva chiusa
 * @return {Array}        array delle continuita aggiustato
 */
function isClosedShape(period){
    if(period >= 0){
        var result = [period];
        for(var i = 0 ; i < param.continuity.length ; i++){
            result.push(param.continuity[i]);
        }
        result.push(period);
        param.continuity = result;

        gc_period(period);

    }else{
        var result = [- 1];
        for(var i = 0 ; i < param.continuity.length ; i++){
            result.push(param.continuity[i]);
        }
        result.push(- 1);
        param.continuity = result;
    }
}

function gc_period(period){
    var nm = param.degree.length;
    var d0 = param.degree[0];
    param.breakPoint = param.breakPoint.reverse();
    param.continuity = param.continuity.reverse();
    param.degree = param.degree.reverse();
    var ccount = d0 - period;
    var i = 0;

    while(ccount < d0 + 1){
        ccount = ccount + param.degree[i] - param.continuity[i + 1];
        param.breakPoint[nm + i + 1] = param.breakPoint[nm] - (param.breakPoint[0] - param.breakPoint[i + 1]);
        param.continuity[nm + i + 1] = param.continuity[i + 1];
        param.degree[nm + i] = param.degree[i];
        i++;
    }
    param.indicePrimoBreakPoint = i;
    param.indiceUltimoBreakPoint = nm + i;
    param.continuity[nm + i] = ccount + param.continuity[nm + i] - d0 - 1;


    param.breakPoint = param.breakPoint.reverse();
    param.continuity = param.continuity.reverse();
    param.degree = param.degree.reverse();

    nm = nm + i - 1;

    d0 = param.degree[nm];
    ccount = d0 - period;

    i = 1;
    while(ccount < d0 + 1){
        ccount = ccount + param.degree[param.indicePrimoBreakPoint + i - 1] - param.continuity[param.indicePrimoBreakPoint + i];
        param.breakPoint[nm + i + 1] = param.breakPoint[param.indiceUltimoBreakPoint] + (param.breakPoint[param.indicePrimoBreakPoint + i] - param.breakPoint[param.indicePrimoBreakPoint]);
        param.continuity[nm + i + 1] = param.continuity[param.indicePrimoBreakPoint + i];
        param.degree[nm + i] = param.degree[param.indicePrimoBreakPoint + i -1];
        i++;
    }

    param.continuity[nm + i] = ccount + param.continuity[nm + i] - d0 - 1;

}

/**
 * Funzione che si occupa di creare i punti valutazione sull intervallo [a,b] indicando in quanti punti deve essere
 * diviso l intervallo
 * @method gc_mesh_new
 * @param  {Integer}    numeroDiPunti Numero di punti da dividere l intervallo
 * @return {Array}                    Array di punti equidistanti
 */
function gc_mesh_new(numeroDiPunti){
      if(numeroDiPunti < 2){
         numeroDiPunti = 2;
      }
      var result = [];
      var lengthPartizione = param.partizioneNodaleT.length;
      var j = 1;

      for(var i = param.indicePrimoBreakPoint; i < param.indiceUltimoBreakPoint; i++){

         result = result.concat((linspace(param.breakPoint[i], param.breakPoint[i + 1], numeroDiPunti)));

      }

      return result;

}

/**
 * Funzione di supporto che crea una successione di punti
 * @method linspace
 * @param  {Integer} a indice d inizio
 * @param  {Integer} b indice di fine
 * @param  {Integer} n numero di punti da dividere
 * @return {Array}   punti equidistanti
 */
function linspace(a,b,n) {
   if(typeof n === "undefined") n = Math.max(Math.round(b-a)+1,1);
   if(n<2) { return n===1?[a]:[]; }
   var i,ret = Array(n);
   n--;
   for(i=n;i>=0;i--) { ret[i] = (i*b+(n-i)*a)/n; }
   return ret;
}

/**
 * Funzione che calcola la matrice bs attraverso la sua formula ricorrente
 * @method gc_MDbspl_valder
 * @param  {Array}         evalutatinPoint Punti di valutazione della funzione ricorrente
 * @return {Matrix}        bs              Matrice bs (evalutatinPoint X numeroBreakPoint)
 */
function gc_MDbspl_valder( evalutatinPoint ){

    var bs = []
    var lenghtPartizioneT = param.partizioneNodaleT.length;
    var scaledPartition = scaled_param();
    console.log(scaledPartition.t, scaledPartition.s);
    var vettoreIntervallinodali = findInt(evalutatinPoint);

    var numberEvalutationPoint = evalutatinPoint.length;
    var temp = [];
    var beta = [];
    var kk = 0;

    for(var i = 0 ; i < numberEvalutationPoint ; i++){
        bs[i] = new Array();
        for(var j = 0 ; j < lenghtPartizioneT; j ++){
            bs[i][j] = new Array();
            for(var z = 0 ; z < kk + 1; z++){
                bs[i][j][z] = 0;

            }
        }
    }


    for(var i = 0 ; i < numberEvalutationPoint ; i++){
      var l = vettoreIntervallinodali[i];
       bs[i][l][0] = 1;

       var ix1 = findInx(evalutatinPoint[i], param.indiciPartizioneNodaleT[l]);

       var x1 = param.breakPoint[ix1];

       var g = param.degree[ix1];
       var gck = Math.min(g , kk);
       var xx = (scaledPartition.paramd.breakPoint[ix1] + (evalutatinPoint[i] - x1) / g);
       var k = l;
       for(var count = 0 ; count < g; count++){
            for(var mycount = 0 ; mycount < count + 1; mycount++){
                temp[mycount]  = 0;
            }

            if(count < g - 1){

                var gr = count + 1;

            }else {

                var gr = 1;

            }

            for(var j = l; j <= k ; j++){

                if(count < g - 1){

                    var d1 =evalutatinPoint[i] - param.partizioneNodaleT[j];
                    var d2 = param.partizioneNodaleS[count + j - g] - evalutatinPoint[i];

                }else{
                    var d1 = xx - scaledPartition.t[j];
                    var d2 = scaledPartition.s[count + j - g] - xx;
                }


                beta[0] = bs[i][j][0]/ (d1 + d2);
                bs[i][j - 1][0] = d2 * beta[0] + temp[0];
                temp[0] = d1 * beta[0];
                var ij = 0;
                for (var gc = g - gck + 1; gc <= count + 1; gc++){

                    ij++;
                    beta[ij] = bs[i][j][ij - 1] / (d1 + d2);
                    bs[i][j - 1][ij] = gr * (temp[ij] - beta[ij]);
                    temp[ij] = beta[ij];

                }
            }

            bs[i][k][0] = temp[0];



            ij = 0;
            for(var gc = g - gck + 1; gc <= count + 1; gc++){
                  ij++;
                bs[i][k][ij] = gr * temp[ij];

            }
            l = l - 1;
        }

    }
    return bs;
}

/**
 * Funzione che si occupa di recuperare l x di un determinato breakPoint indicato nel campo indice
 * @method findInx
 * @param  {Integer} point  punto da cercare
 * @param  {Integer} indice Indice del intervallo nodale dove si trova
 * @return {Integer}         Valore del breakPoint
 */
function findInx(point, indice){

    var numberBreakPoint = _.cloneDeep(param.numeroBreakPoint);
    var m = 1
    var k = _.cloneDeep(param.indiceUltimoBreakPoint);
    for(var i = 0 ; i < m; i++){
        var l = indice;
        while((k - l) > 1){
            var mid = Math.floor((l +k)/2);
            if(point < param.breakPoint[mid])
                k = mid;
            else {
                l = mid;
            }
        }

    }
    return l;

}

/**
 * Funzione che si occupa di recuperare l indice del intervallo nodale T di un punto o una serie di punti
 * @method findInt
 * @param  {Array} evalutatinPoint  Punti di cui cercare il relativo indice in t
 * @return {Array}                 Array formato da indici indicante per ogni punto di valutazione il corrispettivo indice in t
 */
function findInt(evalutatinPoint){
    var t = _.cloneDeep(param.partizioneNodaleT);
    var nt = (param.partizioneNodaleT.length);
    var result = [];

    t.push(param.estremoB);
    nt = nt + 1;
    var nm = param.breakPoint.length;
    var m = evalutatinPoint.length;
    var k = 0;

    for(var i = 0 ; i < m ; i++){
        result[i] = 0;
        k = nt - 1;
        while (k - result[i] > 1) {
            var mid = Math.trunc((result[i] + k)/2);
            if(evalutatinPoint[i] < t[mid])
                k = mid;
            else {
                result[i] = mid;
            }
        }
    }
    return result;

}

/**
 * Funzione che si occupa di scalare la partizione nodale t e s
 * @method scaled_param
 * @return {[type]}     [description]
 */

function scaled_param(){
    var paramd = _.cloneDeep(param);

    var nm = paramd.breakPoint.length;
    var t = [];
    var s = [];

    paramd.breakPoint[0] = 0;

    for(var i = 1 ; i < nm ; i++){
        paramd.breakPoint[i] = paramd.breakPoint[i - 1] + (param.breakPoint[i] - param.breakPoint[i - 1]) / param.degree[i - 1];

    }
    for(var i = 0; i < param.indiceUltimoBreakPoint; i++){

            for(var j = 0 ; j < (param.degree[i] - param.continuity[i]); ++j){
                t.push(paramd.breakPoint[i]);

            }
    }
    var ij = 0;
    for(var i = param.indicePrimoBreakPoint ; i < param.degree.length; i++ ){
        for(var j = 0 ; j < (param.degree[i] - param.continuity[i + 1]) ; j++ ){
            s[ij] = (paramd.breakPoint[i + 1]);
            ij++;
        }
    }

    return {t: t , s: s , paramd: paramd};

}

/**
 * Funzione che si occupa di creare le partizioni Nodali T e S per ogni breakPoint
 * @method partizioniNodali
 * @return {Array}        La struttura param aggiornata
 */
function partizioniNodali(){
    var ij = 0;
    for(var i = 0; i <= param.indiceUltimoBreakPoint - 1; i++){
            for(var j = 0 ; j < (param.degree[i] - param.continuity[i]); j++){
                param.partizioneNodaleT[ij] = param.breakPoint[i];
                param.indiciPartizioneNodaleT[ij] = i;
                ij++;
            }

    }

    ij = 0;
    for(var i = param.indicePrimoBreakPoint; i < param.degree.length; i++ ){
        for(var j = 0 ; j < (param.degree[i] - param.continuity[i + 1]) ; j++ ){
            param.partizioneNodaleS[ij] = (param.breakPoint[i + 1]);
            param.indiciPartizioneNodaleS[ij] =  i + 1 ;
            ij++;
        }
    }
}
