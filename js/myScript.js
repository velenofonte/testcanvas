var canvas = document.getElementById('canvas');
var ctx = canvas.getContext("2d");
var element = [];
var pointLine = [];
var IDelement = -1;
var nPoint = 0;
var bs = [];
const NUMBER_POINT = 20;


ctx.lineJoin = 'round';
canvas.addEventListener('mouseup', mouseUpFunction);
canvas.addEventListener('mousedown', mouseDownFunction);
canvas.addEventListener('mousemove', mouseMoveFunction);
document.addEventListener('keypress', drawLine);

$('#clearCanvas').on('click',function(e){
    element = [];
    $('#point').empty();
    ctx.clearRect(0, 0, canvas.width, canvas.height);
});

/**
 * Funzione che controlla se il numero di controlPoint raggiunge il numero minimo
 * poi chiama la funzione mainVD_MDspl_new che costruisce la matrice bs (inserendo la creazione della matrice in questa funzione viene calcolata solo quando viene inserito un nuovo controlPoint)
 * @method drawLine
 * @return {[type]} Ritorna la curva disegnata
 */

function drawLine(){

    if(element.length < 4){
        alert('devi inserire almeno 4 punti');
        return;
    }
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    bs =  mainVD_MDspl_new(element.length, NUMBER_POINT , 0);
    drawLineRedraw();


}

/**

 * Funzione che disegna una curva attraverso i punti forniti in un array
 * @return curva
 */
function drawLineRedraw() {

    for (var i = 0; i < element.length - 1; i++) {
        createPoint(element[i],'black');
        ctx.beginPath();
        ctx.moveTo(element[i].x, element[i].y);
        ctx.lineTo(element[i + 1].x, element[i + 1].y);
        ctx.stroke();

    }
    createPoint(element[i],'black');

    //funzione che data la matrice e i controlPoint fa la moltiplicazione matrice vettore
    var calculatedPoint = calculateMatrixControl(bs,element);


    for(var i = 0 ; i < calculatedPoint.length; i++){
        if(i % NUMBER_POINT == 0  && i != 0 )
            createPoint(calculatedPoint[i], 'red');
        else
            createPoint(calculatedPoint[i], 'green');
    }


}


/**
 * Funzione che riconosce se e qualeelemento della canvas e stato cliccato
 * @param  {Event} e    Evento MouseDown
 * @return {Void}       Setta al variabile globale IDelement
 */
function mouseDownFunction(e) {

    IDelement = intersect(e);

}

/**
 * Funzione che implemente il Mousemove da parte dell utente questa funzione Se un elemento e stato clickatto
 * ne aggiorna la posizione nell vettore element contente tutte le coordinate xy degli oggetti della canvas
 *
 * @param  {Event} e [description]
 * @return {[type]}   [description]
 */
function mouseMoveFunction(e) {

    var lineArray = -1;
    lineArray = intersectPointLine(e);
    if (lineArray !== -1) {
        //colorLine(lineArray);
    }


    if (IDelement !== -1) {

        //aggiorno la posizione dell elemento in posizione IDelement
        element[IDelement] = getMousePos(e);

        //recupero l id del punto e aggiorno le informazioni del pannello sinitro
        var numeroDelPunto = $('#' + IDelement).attr('id');
        $('#' + IDelement).text('Control Point n ' + numeroDelPunto + ' Coordinata x: ' + Math.round(element[IDelement].x) + ' Coordinata y: ' + Math.round(element[IDelement].y));

        // Ridisegno tutto
        redraw();

    }
}


/**
 * Funzione che pulisce la canvas e chima le due funzioni che rispettivamente ricreeranno i punti e il disegno
 * @return {[type]} [description]
 */
function redraw() {

    //funzione che pulisce la canvas
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawLineRedraw();
}

/**
 * Funzione che preso un evento e controlla se il click dell utente ha interessato un nostro elemento della canvas
 * qui l elemento e definito dalle sue coordinate xy e dalla sua grandezza quindi se le coordinate del click rientrano in questa area
 * allora e stato preso
 *
 * @param  {Event} e    Evento mouse down
 * @return {Integer} i  Index del elemento cliccato oppure -1 in caso contrario
 */
function intersect(e) {
    var mousePosition = getMousePos(e);
    for (var i = 0; i < element.length; i++) {
        if (mousePosition.x < element[i].x + 10 && mousePosition.x > element[i].x - 10 && mousePosition.y < element[i].y + 10 && mousePosition.y > element[i].y - 10) {
            return i;
        }
    }
    return -1
}

/**
 * funzione che ridisegna tutti i punti tenendo in considerazione se l utente e passato sopra un segmento
 *
 * @param  {int} arrayIndex indice che indica quali punti colorare in modo diverso
 * @return {void}            disegna i punti
 */
function colorLine(arrayIndex) {

    //funzione che pulisce la canvas
    //ctx.clearRect(0, 0, canvas.width, canvas.height);
    for( var count = 0 ; count < element.length ; count ++ ){
        createPoint(element[count],'black');
    }
    for (var k = 0; k < pointLine.length; k++) {
        for (var j = 0; j < pointLine[k].length; j++) {
            if (k == arrayIndex) {
                createPoint(pointLine[k][j], 'red');
            } else {
                createPoint(pointLine[k][j], 'green');
            }
        }
    }
}
/**
 * funzione che riesce a catchare la posione del mouse attraverso l evento "e"
 * e confronta con l array dei punti
 * @param  {event} e    contiene la posizione del click
 * @return {int}    index che contiene l indice del segmento di cui l elemento cliccato fa pare
 */
function intersectPointLine(e) {
    var mousePosition = getMousePos(e);
    try {
        pointLine.forEach(function(element2, index) {
            for (var i = 1; i < element2.length; i++) {
                if (mousePosition.x < element2[i].x + 10 && mousePosition.x > element2[i].x - 10 && mousePosition.y < element2[i].y + 10 && mousePosition.y > element2[i].y - 10) {
                    throw index;
                }
            }
        });
        return -1;
    } catch (err) {
        return err;
    }

}

/**
 * Funzione che implementa l envento 'mouseup'. Se c e un elemento selezionato lo deleseleziona altrimenti
 * crea un nuovo punto sulla canvas e appende alla lista dei punti il nuovo punto
 *
 * @param  {Event} e    Evento mouseup
 * @return {Void}      deseleziona un oggetto e appende una <li> nella <ul> dei punti
 */
function mouseUpFunction(e) {
    e.preventDefault();
    if (IDelement !== -1) {
        IDelement = -1;
        return;
    }
    var mouse = getMousePos(e);
    createPoint(mouse, 'black');
    element.push(mouse);
    $('#point').append('<li id=' + nPoint + '> Control Point n ' + nPoint + ' Coordinata x:' + Math.round(mouse.x) + ' Coordinata y:' + Math.round(mouse.y) + '</li>');
    nPoint++;
}

/**
 * window to view
 * Funzione che recupera le coordinate del mouse e le scala
 * @param  {Evento} evt     che viene passata dalla funzione chiamante
 * @return {Object} {x: Integer, y: Integer}    posizione corretta del mouse
 */
function getMousePos(evt) {
    var rect = canvas.getBoundingClientRect(),
        scaleX = canvas.width / rect.width,
        scaleY = canvas.height / rect.height;

    return {
        x: Math.round((evt.clientX - rect.left) * scaleX),
        y: Math.round((evt.clientY - rect.top) * scaleY)
    }
}

/**
 * Semplice funzione che crea un punto data una posizione xy
 * @param  {Object} position    {x: Integer, y: Integer}
 * @return {Void}               Disegna un punto sulla canvas
 */
function createPoint(position, color) {
    ctx.beginPath();
    if (color !== 'black')
        ctx.arc(position.x, position.y, 5, 0, 2 * Math.PI, false);
    else {

        ctx.arc(position.x, position.y, 10, 0, 2 * Math.PI, false);
    }

    ctx.fillStyle = color;
    ctx.fill();
    ctx.stroke();
}
